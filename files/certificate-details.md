# Certificate details

Generated with the following code and some manual cleanup:

```bash
for i in files/*.crt; do
    echo "## $i"
    csplit -szf /tmp/${i##*/}- $i '/END CERTIFICATE/+1'
    for j in /tmp/${i##*/}-*; do
        openssl x509 -noout -in $j -text
    done
done
```

## files/rh-cki.crt

```text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            3a:ca:98:8d:82:02:57:3d:f6:62:79:b8:dd:e6:65:85:31:a0:df:f0
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: CN = CKI CA
        Validity
            Not Before: Jun 26 12:53:08 2020 GMT
            Not After : Oct 28 12:53:08 3019 GMT
        Subject: CN = CKI CA
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus: ...
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints:
                CA:TRUE
            X509v3 Key Usage:
                Certificate Sign, CRL Sign
```

## files/rh-prod.crt

```text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 6 (0x6)
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: O = Red Hat, OU = prod, CN = Intermediate Certificate Authority
        Validity
            Not Before: Oct 14 17:47:56 2015 GMT
            Not After : Oct  9 17:47:56 2035 GMT
        Subject: O = Red Hat, OU = prod, CN = Certificate Authority
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus: ...
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Authority Key Identifier:
                keyid:30:DE:04:54:A1:86:02:9F:11:FA:50:F9:05:70:A7:28:C0:C0:4F:FC
            X509v3 Basic Constraints: critical
                CA:TRUE, pathlen:0
            X509v3 Key Usage: critical
                Digital Signature, Non Repudiation, Certificate Sign, CRL Sign
            X509v3 Subject Key Identifier:
                7B:DA:09:F5:49:5D:D9:D7:5C:C9:36:F8:55:D2:1B:97:9E:11:2F:7E
            Authority Information Access:
                OCSP - URI:http://ocsp.redhat.com/ca/intocsp/
```

## files/rh-prod-intermediate.crt

```text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 20 (0x14)
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = US, ST = North Carolina, L = Raleigh, O = "Red Hat, Inc.", OU = Red Hat IT, CN = Red Hat IT Root CA, emailAddress = infosec@redhat.com
        Validity
            Not Before: Oct 14 17:29:07 2015 GMT
            Not After : Oct  6 17:29:07 2045 GMT
        Subject: O = Red Hat, OU = prod, CN = Intermediate Certificate Authority
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus: ...
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier:
                30:DE:04:54:A1:86:02:9F:11:FA:50:F9:05:70:A7:28:C0:C0:4F:FC
            X509v3 Authority Key Identifier:
                keyid:7E:D1:E3:20:BE:51:E8:48:20:F7:AE:2F:C6:6A:C3:73:F3:6E:07:19
            X509v3 Basic Constraints: critical
                CA:TRUE, pathlen:1
            X509v3 Key Usage: critical
                Digital Signature, Certificate Sign, CRL Sign
            Netscape Cert Type:
                SSL CA, S/MIME CA
```

## files/rh-root.crt

```text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 2022041053 (0x7885e5dd)
        Signature Algorithm: sha384WithRSAEncryption
        Issuer: C = US, ST = North Carolina, L = Raleigh, O = "Red Hat, Inc.", OU = Red Hat IT, CN = Internal Root CA, emailAddress = infosec@redhat.com
        Validity
            Not Before: Apr 10 13:17:18 2022 GMT
            Not After : Apr  2 13:17:18 2052 GMT
        Subject: C = US, ST = North Carolina, L = Raleigh, O = "Red Hat, Inc.", OU = Red Hat IT, CN = Internal Root CA, emailAddress = infosec@redhat.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (4096 bit)
                Modulus: ...
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier:
                B5:FA:8D:E5:0A:78:A1:09:95:DB:4D:21:86:95:6A:59:4F:49:C8:8B
            X509v3 Authority Key Identifier:
                keyid:B5:FA:8D:E5:0A:78:A1:09:95:DB:4D:21:86:95:6A:59:4F:49:C8:8B
            X509v3 Basic Constraints: critical
                CA:TRUE
            X509v3 Key Usage: critical
                Digital Signature, Certificate Sign, CRL Sign
            X509v3 CRL Distribution Points:
                Full Name:
                  URI:http://oscp.redhat.com/crl.pem
```

```text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            db:a7:23:40:fa:eb:67:27
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = US, ST = North Carolina, L = Raleigh, O = "Red Hat, Inc.", OU = Red Hat IT, CN = Red Hat IT Root CA, emailAddress = infosec@redhat.com
        Validity
            Not Before: Jul  6 17:38:11 2015 GMT
            Not After : Jun 26 17:38:11 2055 GMT
        Subject: C = US, ST = North Carolina, L = Raleigh, O = "Red Hat, Inc.", OU = Red Hat IT, CN = Red Hat IT Root CA, emailAddress = infosec@redhat.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus: ...
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier:
                7E:D1:E3:20:BE:51:E8:48:20:F7:AE:2F:C6:6A:C3:73:F3:6E:07:19
            X509v3 Authority Key Identifier:
                keyid:7E:D1:E3:20:BE:51:E8:48:20:F7:AE:2F:C6:6A:C3:73:F3:6E:07:19
            X509v3 Basic Constraints: critical
                CA:TRUE
            X509v3 Key Usage: critical
                Digital Signature, Certificate Sign, CRL Sign
```
