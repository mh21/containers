/* DO NOT use Dockerfile/Containerfile commands before this line */
#include "setup-from-fedora"

/* Tools to build container images */
RUN dnf -y install buildah runc cpp qemu-user-static lolcat toilet

/* Tools to build AMIs via an osbuild-composer service container */
RUN dnf install -y composer-cli socat jq awscli

/* Tools to pull/push container images */
RUN dnf -y install skopeo

/* Force the use of fully qualified image names everywhere by Disabling registry search */
RUN sed -i -E 's/^(unqualified-search-registries).*/\1 = []/g' /etc/containers/registries.conf

/* Install the build script and includes */
RUN mkdir -p /usr/local/bin /usr/local/share/cki/buildah
COPY cki_build_image.sh /usr/local/bin
COPY "includes/"* /usr/local/share/cki/buildah

ADD https://raw.githubusercontent.com/qemu/qemu/master/scripts/qemu-binfmt-conf.sh /usr/local/bin/qemu-binfmt-conf.sh
RUN chmod +x /usr/local/bin/qemu-binfmt-conf.sh

/* Experimental z14 support via qemu-s390x version 6.0.50 (v2.9.0-34932-gd4eaebd73a).
 * The binary was produced via:
 *   podman run --rm -it -v .:/code quay.io/cki/python
 *   dnf builddep qemu-user-static
 *   dnf install ninja-build
 *   git clone git@github.com:davidhildenbrand/qemu.git
 *   cd qemu
 *   git checkout z14 // d4eaebd73ab40283eeba3a2c8dfc197b051fa537
 *   mkdir build
 *   cd build
 *   ../configure --without-default-features --enable-linux-user --target-list=s390x-linux-user --static --disable-pie --disable-capstone
 *   make -j 8
 */
COPY files/qemu-s390x-static /usr/bin

/* Experimental power9 support via qemu-ppc64le version 6.0.50 (v6.0.0-2064-g13d5f87cc3).
 * The binary was produced via:
 *   podman run --rm -it -v .:/code quay.io/cki/python
 *   dnf builddep qemu-user-static
 *   dnf install ninja-build
 *   git clone https://gitlab.com/qemu-project/qemu.git
 *   cd qemu
 *   mkdir build
 *   cd build
 *   ../configure --without-default-features --enable-linux-user --target-list=ppc64le-linux-user --static --disable-pie --disable-capstone
 *   make -j 8
 */
COPY files/qemu-ppc64le-static /usr/bin

/* Place /var/lib/containers outside the root overlayfs.
 * This works around the error
 *    "mount `/var/lib/containers/.../merge` to `/logs`: Invalid * argument"
 * This has been filed as https://github.com/containers/buildah/issues/3281.
 * The fix has been adapted from the manifest of quay.io/buildah/stable:v1.21.0.
 */
VOLUME /var/lib/containers

#include "cleanup"
